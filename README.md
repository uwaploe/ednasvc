## eDNA Sampler User Interface Server

This application provides a small HTTP server which implements the backend of the user-interface for the eDNA Sampler. The UI front-end is served from `/` as a single page application. It is designed to run as a [Systemd](https://systemd.io) service.

### API

See the `api.yaml` file for an [OpenAPI](https://swagger.io/docs/specification/about/) description.
