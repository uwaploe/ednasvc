module bitbucket.org/uwaploe/ednasvc

go 1.13

require (
	github.com/coreos/go-systemd v0.0.0-20191104093116-d3cd4ed1dbcf
	github.com/coreos/go-systemd/v22 v22.1.0
	github.com/felixge/httpsnoop v1.0.1
	github.com/gorilla/mux v1.8.0
	github.com/mitchellh/go-homedir v1.1.0
)
