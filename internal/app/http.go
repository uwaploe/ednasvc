package app

import (
	"encoding/json"
	"io"
	"net/http"
	"os"
	"path/filepath"
	"strings"

	"github.com/felixge/httpsnoop"
)

// Borrowed from: https://github.com/gorilla/mux

// spaHandler implements the http.Handler interface, so we can use it
// to respond to HTTP requests. The path to the static directory and
// path to the index file within that static directory are used to
// serve the SPA in the given static directory.
type spaHandler struct {
	staticPath string
	indexPath  string
}

// ServeHTTP inspects the URL path to locate a file within the static dir
// on the SPA handler. If a file is found, it will be served. If not, the
// file located at the index path on the SPA handler will be served. This
// is suitable behavior for serving an SPA (single page application).
func (h spaHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	// get the absolute path to prevent directory traversal
	path, err := filepath.Abs(r.URL.Path)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// prepend the path with the path to the static directory
	path = filepath.Join(h.staticPath, path)

	// check whether a file exists at the given path
	_, err = os.Stat(path)
	if os.IsNotExist(err) {
		// file does not exist, serve index.html
		http.ServeFile(w, r, filepath.Join(h.staticPath, h.indexPath))
		return
	} else if err != nil {
		// if we got an error (that wasn't that the file doesn't exist) stating the
		// file, return a 500 internal server error and stop
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// otherwise, use http.FileServer to serve the static dir
	http.FileServer(http.Dir(h.staticPath)).ServeHTTP(w, r)
}

// Auth provides the credentials for BasicAuth
type Auth struct {
	User     string
	Password string
	Realm    string
}

// BasicAuth returns a Handler which implements single-user HTTP Basic
// Authentication for the Handler h.
func BasicAuth(h http.Handler, a Auth) http.Handler {
	return &authHandler{
		handler: h,
		auth:    a,
	}
}

type authHandler struct {
	handler http.Handler
	auth    Auth
}

func (h *authHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	u, p, ok := r.BasicAuth()
	if !ok || u != h.auth.User || p != h.auth.Password {
		h.unauthorised(w)
		return
	}
	h.handler.ServeHTTP(w, r)
}

func (h *authHandler) unauthorised(w http.ResponseWriter) {
	w.Header().Set("WWW-Authenticate", "Basic realm="+h.auth.Realm)
	w.WriteHeader(http.StatusUnauthorized)
}

// HTTPReqInfo describes info about HTTP request
type HTTPReqInfo struct {
	Method    string `json:"method"`
	Uri       string `json:"uri"`
	Referer   string `json:"referer"`
	Ipaddr    string `json:"ipaddr"`
	Code      int    `json:"code"`
	Size      int64  `json:"size"`
	Duration  string `json:"duration"`
	UserAgent string `json:"user_agent"`
}

func ipAddrFromRemoteAddr(s string) string {
	idx := strings.LastIndex(s, ":")
	if idx == -1 {
		return s
	}
	return s[:idx]
}

// LogRequest wraps an exiting http.Handler in order to log information about
// each request and write the information to an io.Writer in JSON format.
func LogRequest(h http.Handler, wtr io.Writer) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		ri := HTTPReqInfo{
			Method:    r.Method,
			Uri:       r.URL.String(),
			Referer:   r.Header.Get("Referer"),
			UserAgent: r.Header.Get("User-Agent"),
		}

		ri.Ipaddr = ipAddrFromRemoteAddr(r.RemoteAddr)

		// this runs handler h and captures information about
		// HTTP request
		m := httpsnoop.CaptureMetrics(h, w, r)

		ri.Code = m.Code
		ri.Size = m.Written
		ri.Duration = m.Duration.String()
		json.NewEncoder(wtr).Encode(ri)
	}
	return http.HandlerFunc(fn)
}
