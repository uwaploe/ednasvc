package app

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"log"
	"net"
	"net/http"
	"os"
	"time"

	"bitbucket.org/uwaploe/ednasvc/internal/data"
	"bitbucket.org/uwaploe/ednasvc/internal/deployment"
	"github.com/coreos/go-systemd/v22/activation"
	"github.com/gorilla/mux"
	homedir "github.com/mitchellh/go-homedir"
)

const Usage = `Usage: ednasvc [options] [addr]

HTTP server to provide the backend for the eDNA Sampler UI.
`

type appEnv struct {
	fl       *flag.FlagSet
	svcAddr  string
	dataDir  string
	outDir   string
	assetDir string
	cfgDir   string
	vers     string
}

var versReq = errors.New("show version")

func expandHome(path string) string {
	newpath, _ := homedir.Expand(path)
	return newpath
}

func (app *appEnv) fromArgs(args []string) error {
	fl := flag.NewFlagSet("ednasvc", flag.ContinueOnError)
	fl.Usage = func() {
		fmt.Fprintf(os.Stderr, Usage)
		fl.PrintDefaults()
	}

	showVers := fl.Bool("version", false, "Show program version information and exit")
	fl.StringVar(&app.dataDir, "datadir",
		lookupEnvOrString("EDNA_DATADIR", expandHome("~/data")),
		"eDNA data directory")
	fl.StringVar(&app.cfgDir, "cfgdir",
		lookupEnvOrString("EDNA_CFGDIR", expandHome("~/config")),
		"location of deployment configuration file")
	fl.StringVar(&app.assetDir, "assets",
		lookupEnvOrString("EDNA_ASSETDIR", expandHome("~/html")),
		"location of UI front-end files")
	fl.StringVar(&app.outDir, "outbox",
		lookupEnvOrString("EDNA_OUTBOX", expandHome("~/OUTBOX")),
		"location of data files")
	app.fl = fl
	if err := fl.Parse(args); err != nil {
		return err
	}

	if *showVers {
		return versReq
	}

	req := fl.Args()
	if len(req) > 0 {
		app.svcAddr = req[0]
	}

	return nil
}

func (app *appEnv) run(ctx context.Context) error {
	mon, err := deployment.NewMonitor(app.cfgDir)
	if err != nil {
		return err
	}

	listeners, err := activation.Listeners()
	if err != nil {
		return fmt.Errorf("Systemd activation error: %w", err)
	}

	var listener net.Listener
	switch n := len(listeners); n {
	case 0:
		if app.svcAddr == "" {
			return errors.New("Server address must be specified")
		}
		// Not activated by a Systemd socket ...
		addr, _ := net.ResolveTCPAddr("tcp", app.svcAddr)
		listener, err = net.ListenTCP("tcp", addr)
		if err != nil {
			return err
		}
	case 1:
		listener = listeners[0]
	default:
		return fmt.Errorf("Unexpected number of socket activation fds: %d", n)
	}

	router := mux.NewRouter()
	router.Handle("/deployment",
		http.TimeoutHandler(mon, 10*time.Second, "Operation timed-out"))
	router.PathPrefix("/outbox").Handler(http.StripPrefix("/outbox/", data.NewOutbox(app.outDir)))
	// Serve the single-page-application (front-end)
	spa := spaHandler{staticPath: app.assetDir, indexPath: "index.html"}
	router.PathPrefix("/").Handler(spa)

	srv := &http.Server{
		Handler:      LogRequest(router, os.Stdout),
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	idleConnsClosed := make(chan struct{})
	go func() {
		<-ctx.Done()

		// Context cancelled, shutdown the server
		if err := srv.Shutdown(context.Background()); err != nil {
			// Error from closing listeners, or context timeout:
			log.Printf("HTTP server Shutdown: %v", err)
		}
		close(idleConnsClosed)
	}()

	log.Printf("eDNA UI backend starting: %s", app.vers)
	if err := srv.Serve(listener); err != http.ErrServerClosed {
		// Error starting or closing listener:
		return err
	}

	<-idleConnsClosed

	return nil
}

func CLI(ctx context.Context, vers string, args []string) int {
	var app appEnv
	app.vers = vers
	err := app.fromArgs(args)
	if err != nil {
		switch err {
		case versReq:
			fmt.Fprintf(os.Stderr, "%s\n", vers)
		}

		return 2
	}
	if err = app.run(ctx); err != nil {
		fmt.Fprintf(os.Stderr, "Runtime error: %v\n", err)
		return 1
	}
	return 0
}
