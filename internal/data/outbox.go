package data

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"path/filepath"
	"sort"
	"strings"
	"syscall"
)

type File struct {
	Name    string `json:"name"`
	Size    int64  `json:"size"`
	Summary Result `json:"summary,omitempty"`
}

type Listing struct {
	Files     []File `json:"files"`
	Freespace uint64 `json:"freespace"`
}

func badPath(name string) bool {
	parts := strings.Split(name, "/")
	for _, part := range parts {
		if strings.HasPrefix(part, ".") {
			return true
		}
	}
	return false
}

// Implement the http.FileSystem interface on a single
// directory and prevent "escapes" using ".." or
// symbolic links.
type Outbox struct {
	dir string
}

// Return a new Oubox attached to dir
func NewOutbox(dir string) Outbox {
	path, _ := filepath.Abs(dir)
	return Outbox{dir: path}
}

// Open implements the http.FileSystem interface
func (o Outbox) Open(name string) (http.File, error) {
	if badPath(name) {
		return nil, os.ErrPermission
	}

	path := filepath.Join(o.dir, name)
	if fi, err := os.Stat(path); err == nil {
		if !fi.Mode().IsRegular() {
			return nil, os.ErrPermission
		}
	}

	return os.Open(path)
}

func (o Outbox) getListing(max int) (Listing, error) {
	l := Listing{Files: make([]File, 0)}
	f, err := os.Open(o.dir)
	if err != nil {
		return l, err
	}
	defer f.Close()

	var stat syscall.Statfs_t
	syscall.Statfs(o.dir, &stat)
	l.Freespace = uint64(stat.Bsize) * stat.Bavail

	fi, err := f.Readdir(0)
	if err != nil {
		return l, err
	}

	// Sort in descending order
	sort.SliceStable(fi, func(i, j int) bool {
		return fi[i].Name() > fi[j].Name()
	})

	for i, info := range fi {
		if !info.Mode().IsRegular() {
			continue
		}
		path := filepath.Join(o.dir, info.Name())
		if i < max {
			summary, err := GetSummary(path)
			if err != nil {
				continue
			}
			l.Files = append(l.Files,
				File{Name: info.Name(), Size: info.Size(), Summary: summary})
		} else {
			l.Files = append(l.Files,
				File{Name: info.Name(), Size: info.Size()})
		}
	}

	return l, nil
}

// ServeHTTP implements the http.Handler interface
func (o Outbox) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		if r.URL.Path != "" && r.URL.Path != "/" {
			http.FileServer(o).ServeHTTP(w, r)
			return
		}

		l, err := o.getListing(10)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(l)
	case "DELETE":
		var err error
		parts := strings.Split(r.URL.Path, "/")
		if parts[0] != "" {
			err = os.Remove(filepath.Join(o.dir, parts[0]))
		} else {
			err = os.Remove(filepath.Join(o.dir, parts[1]))
		}
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.WriteHeader(http.StatusOK)
	default:
		http.Error(w, fmt.Sprintf("Invalid method: %q", r.Method),
			http.StatusMethodNotAllowed)
	}

}
