package data

import (
	"path/filepath"
	"testing"
)

func TestDeploymentSummary(t *testing.T) {
	id := "20201107T234941"
	r, err := GetSummary(filepath.Join("testdata",
		"edna_"+id+".tar.gz"))
	if err != nil {
		t.Fatal(err)
	}

	if r.ID != id {
		t.Errorf("Bad ID; expected %q, got %q", id, r.ID)
	}

	if len(r.Samples) != 3 {
		t.Errorf("Bad sample count; expected 3, got %d", len(r.Samples))
	}

}

func TestListing(t *testing.T) {
	ob := NewOutbox("testdata")
	l, err := ob.getListing(10)
	if err != nil {
		t.Fatal(err)
	}

	if len(l.Files) != 1 {
		t.Errorf("Bad file count; expected 1, got %d", len(l.Files))
	}

	id := "20201107T234941"
	r := l.Files[0].Summary
	if r.ID != id {
		t.Errorf("Bad ID; expected %q, got %q", id, r.ID)
	}

	if l.Files[0].Size == 0 {
		t.Errorf("Missing file size: %#v", l.Files[0])
	}

	if l.Freespace == 0 {
		t.Error("Missing free-space value")
	}
}
