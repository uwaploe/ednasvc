// Package data parses data files from the eDNA sampler
package data

import (
	"archive/tar"
	"compress/gzip"
	"encoding/json"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"
)

type SampleStatus struct {
	Index   int     `json:"index"`
	Elapsed float32 `json:"elapsed"`
	Amount  float32 `json:"amount"`
	Pr      float32 `json:"pr"`
	PrOk    bool    `json:"pr_ok"`
	Depth   float32 `json:"depth"`
}

type Sample struct {
	Index        int     `json:"index"`
	Elapsed      float32 `json:"elapsed"`
	Vwater       float32 `json:"vwater"`
	Vethanol     float32 `json:"vethanol"`
	Overpressure bool    `json:"overpressure"`
	Deptherror   bool    `json:"deptherror,omitempty"`
}

type Record struct {
	T     time.Time       `json:"t"`
	Event string          `json:"event"`
	Data  json.RawMessage `json:"data"`
}

// Decode the data property into the value pointed at by v.
func (r Record) Scan(v interface{}) error {
	return json.Unmarshal(r.Data, v)
}

type Result struct {
	ID      string   `json:"id"`
	Samples []Sample `json:"samples"`
}

func getSamples(rdr io.Reader) ([]Sample, error) {
	dec := json.NewDecoder(rdr)
	samples := make([]Sample, 0)
	for {
		var (
			rec    Record
			sample Sample
		)

		err := dec.Decode(&rec)
		if err == io.EOF {
			break
		}
		if err != nil {
			return samples, err
		}
		if strings.HasPrefix(rec.Event, "result.") {
			rec.Scan(&sample)
			sample.Index, _ = strconv.Atoi(rec.Event[7:])
			samples = append(samples, sample)
		}
	}

	return samples, nil
}

func filenameOk(name string) bool {
	if len(name) != 27 {
		return false
	}
	if !strings.HasPrefix(name, "edna_") {
		return false
	}
	if !strings.HasSuffix(name, ".tar.gz") {
		return false
	}

	return true
}

// GetSummary extracts a summary of the deployment results from a
// compressed TAR archive file.
func GetSummary(pathname string) (Result, error) {
	filename := filepath.Base(pathname)
	if !filenameOk(filename) {
		return Result{}, fmt.Errorf("Invalid filename: %q", filename)
	}

	// File name is edna_YYYYmmddTHHMMSS.tar.gz
	r := Result{ID: filename[5:20]}

	f, err := os.Open(pathname)
	if err != nil {
		return r, err
	}
	defer f.Close()

	zr, err := gzip.NewReader(f)
	if err != nil {
		return r, err
	}

	tr := tar.NewReader(zr)
	dfile := "edna_" + r.ID + ".ndjson"
	for {
		hdr, err := tr.Next()
		if err == io.EOF {
			break
		}
		if err != nil {
			return r, err
		}
		if strings.HasSuffix(hdr.Name, dfile) {
			r.Samples, err = getSamples(tr)
			break
		}
	}

	return r, err
}
