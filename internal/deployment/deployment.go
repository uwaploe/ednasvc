// Package deployment provides an http.Handler to monitor the lifecycle
// of the Systemd service which runs the sampler.
//
// http.Handle("/deployment", deployment.NewMonitor(cfgdir))
//
package deployment

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"text/template"

	"github.com/coreos/go-systemd/v22/dbus"
)

const configTemplate = `# eDNA Sampler deployment file
[Deployment]
SeekErr = {{.SeekErr}}
DepthErr = {{.DepthErr}}
PrRate = {{.PrRate}}
SeekTime = {{.SeekTime}}
Downcast = {{.Downcast}}
{{if .Metadata}}
[Metadata]
{{.Metadata | fmtMeta}}
{{- end -}}
{{range $index, $elem := .Depths}}
[Sample.{{$index | plusOne}}]
Depth = {{$elem -}}
{{end}}

[Collect.Sample]
Amount = {{.Collect.SampAmount | printf "%.3f"}}
Time = {{.Collect.SampTime}}

[Collect.Ethanol]
Amount = {{.Collect.EthAmount | printf "%.3f"}}
Time = {{.Collect.EthTime}}
{{- if .Collect.MaxPressure}}
[Pressure.Filter]
Max = {{.Collect.MaxPressure -}}
{{end}}
`

type appHandler func(http.ResponseWriter, *http.Request) error

type SampleCollection struct {
	SampAmount  float32 `json:"s_amount"`
	SampTime    int     `json:"s_time"`
	EthAmount   float32 `json:"e_amount"`
	EthTime     int     `json:"e_time"`
	MaxPressure float32 `json:"maxpr"`
}

type Config struct {
	SeekErr  float32          `json:"seekerr"`
	DepthErr float32          `json:"deptherr"`
	PrRate   float32          `json:"prrate"`
	SeekTime int              `json:"seektime"`
	Downcast bool             `json:"downcast"`
	Depths   []float32        `json:"depths"`
	Metadata []string         `json:"metadata"`
	Collect  SampleCollection `json:"collect"`
}

func (c Config) validate() error {
	return nil
}

func fmtMeta(md []string) string {
	var b strings.Builder
	for i := 0; i < len(md); i += 2 {
		fmt.Fprintf(&b, "%s = %s\n", md[i], md[i+1])
	}
	return b.String()
}

func initTemplate() (*template.Template, error) {
	funcMap := template.FuncMap{
		"plusOne": func(n int) int { return n + 1 },
		"fmtMeta": fmtMeta,
	}
	return template.New("config").Funcs(funcMap).Parse(configTemplate)
}

type Monitor struct {
	unitName string
	conn     *dbus.Conn
	tmpl     *template.Template
	cfgDir   string
}

// NewMonitor creates a new Monitor instance and assigns the directory
// in which to store the configuration file.
func NewMonitor(cfgDir string) (Monitor, error) {
	m := Monitor{
		unitName: "runedna@deploy.service",
		cfgDir:   cfgDir,
	}
	var err error

	m.tmpl, err = initTemplate()
	if err != nil {
		return m, err
	}

	// The service runs under a user Systemd instance
	m.conn, err = dbus.NewUserConnection()
	return m, err
}

func (m Monitor) unitStatus() (dbus.UnitStatus, error) {
	var s dbus.UnitStatus
	status, err := m.conn.ListUnitsByNames([]string{m.unitName})
	if err != nil || len(status) == 0 {
		return s, err
	}
	return status[0], nil
}

func (m Monitor) unitActive() bool {
	status, err := m.unitStatus()
	if err != nil {
		return false
	}
	return status.ActiveState == "active"
}

func (m Monitor) startUnit(cfg Config, result chan<- string) error {
	if m.unitActive() {
		return errors.New("Deployment is active")
	}

	f, err := os.Create(filepath.Join(m.cfgDir, "deploy.cfg"))
	if err != nil {
		return err
	}
	err = m.tmpl.Execute(f, cfg)
	if err != nil {
		return err
	}
	f.Close()

	_, err = m.conn.StartUnit(m.unitName, "replace", result)
	return err
}

func (m Monitor) stopUnit(result chan<- string) error {
	if !m.unitActive() {
		result <- "done"
		return nil
	}
	_, err := m.conn.StopUnit(m.unitName, "replace", result)
	return err
}

func (m Monitor) handleStart(rw http.ResponseWriter, req *http.Request) error {
	var cfg Config
	err := json.NewDecoder(req.Body).Decode(&cfg)
	if err != nil {
		return err
	}

	ch := make(chan string, 1)
	err = m.startUnit(cfg, ch)
	if err != nil {
		return err
	}

	ctx := req.Context()
	select {
	case <-ctx.Done():
		return ctx.Err()
	case result := <-ch:
		if result != "done" {
			return fmt.Errorf("Service not started; result: %q", result)
		}
	}

	status, err := m.unitStatus()
	if err != nil {
		return err
	}

	rw.Header().Set("Content-Type", "application/json")
	json.NewEncoder(rw).Encode(status)

	return nil
}

func (m Monitor) handleStop(rw http.ResponseWriter, req *http.Request) error {
	ch := make(chan string, 1)
	err := m.stopUnit(ch)
	if err != nil {
		return err
	}

	ctx := req.Context()
	select {
	case <-ctx.Done():
		return ctx.Err()
	case result := <-ch:
		if result != "done" {
			return fmt.Errorf("Service not stopped; result: %q", result)
		}
	}

	status, err := m.unitStatus()
	if err != nil {
		return err
	}

	rw.Header().Set("Content-Type", "application/json")
	json.NewEncoder(rw).Encode(status)

	return nil
}

func (m Monitor) ServeHTTP(rw http.ResponseWriter, req *http.Request) {
	switch req.Method {
	case "GET":
		status, err := m.unitStatus()
		if err != nil {
			http.Error(rw, err.Error(), http.StatusInternalServerError)
			return
		}
		rw.Header().Set("Content-Type", "application/json")
		json.NewEncoder(rw).Encode(status)
	case "POST":
		if err := m.handleStart(rw, req); err != nil {
			http.Error(rw, err.Error(), http.StatusInternalServerError)
			return
		}
	case "DELETE":
		if err := m.handleStop(rw, req); err != nil {
			http.Error(rw, err.Error(), http.StatusInternalServerError)
			return
		}
	default:
		http.Error(rw, fmt.Sprintf("Invalid method: %q", req.Method),
			http.StatusMethodNotAllowed)
	}
}
