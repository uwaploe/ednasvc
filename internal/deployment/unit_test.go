package deployment

import (
	"strings"
	"testing"
)

const CONTENTS = `# eDNA Sampler deployment file
[Deployment]
SeekErr = 5
DepthErr = 5
PrRate = 4
SeekTime = 120
Downcast = true

[Metadata]
Cruise = Hake Survey 2021
Cast = test-001

[Sample.1]
Depth = 10
[Sample.2]
Depth = 20
[Sample.3]
Depth = 30

[Collect.Sample]
Amount = 0.200
Time = 60

[Collect.Ethanol]
Amount = 0.010
Time = 20
`

const EXTRA = `[Pressure.Filter]
Max = 12.3
`

var cfgInput Config = Config{
	SeekErr:  5,
	DepthErr: 5,
	PrRate:   4,
	SeekTime: 120,
	Downcast: true,
	Depths:   []float32{10, 20, 30},
	Metadata: []string{"Cruise", "Hake Survey 2021", "Cast", "test-001"},
	Collect: SampleCollection{
		SampAmount: 0.2,
		SampTime:   60,
		EthAmount:  0.01,
		EthTime:    20,
	},
}

func TestTemplate(t *testing.T) {
	tmpl, err := initTemplate()
	if err != nil {
		t.Fatal(err)
	}

	var wtr strings.Builder
	err = tmpl.Execute(&wtr, cfgInput)
	if err != nil {
		t.Fatal(err)
	}

	out := wtr.String()
	if out != CONTENTS {
		t.Errorf("Output mismatch; expected %q, got %q", CONTENTS, out)
	}

}

func TestTemplate2(t *testing.T) {
	tmpl, err := initTemplate()
	if err != nil {
		t.Fatal(err)
	}

	var wtr strings.Builder
	cfgInput.Collect.MaxPressure = 12.3
	err = tmpl.Execute(&wtr, cfgInput)
	if err != nil {
		t.Fatal(err)
	}

	out := wtr.String()
	expect := CONTENTS + EXTRA
	if out != expect {
		t.Errorf("Output mismatch; expected %q, got %q", expect, out)
	}

}
