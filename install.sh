#!/usr/bin/env bash

PROG="ednasvc"
BINDIR="$HOME/.local/bin"
SVCDIR="$HOME/.config/systemd/user"

systemctl --user stop ednasvc.service
systemctl --user stop ednasvc.socket

mkdir -p "$BINDIR"
cp -v $PROG "$BINDIR"

mkdir -p "$SVCDIR"
cp -v systemd/* "$SVCDIR"

systemctl --user daemon-reload
systemctl --user enable --now ednasvc.socket
